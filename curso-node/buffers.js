/*
Buffers
Una tira de bytes ( datos binarios )
Similar a un array de enteros 
tamaño fijo
manipular datos directamente 
sockets
streams
implementar protrocolos complejos 
manipulacion de ficheros/imagenes
criptografia
*/
"use strict";

// var buf = new Buffer(100),
//     buf2 = new Buffer(26),
//     str = "\u00bd + \u00bc = \u00be"; 

//    buf.write("abcd",0,4,"ascii");
//    console.log(buf.toString("ascii"));

var buf = Buffer.alloc(100),
    buf2 = Buffer.alloc(26),
    str = "\u00bd" + "\u00bc" + " = " + "\u00be"; 

   buf.write("abcd",0,4,"ascii");
   buf2.write(str,0,4, "ascii");
   console.log(buf.toString("ascii"));
   console.log(buf2.toString("ascii"))
